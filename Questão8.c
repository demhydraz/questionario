#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <err.h>

// Strings char* começam no pointer e terminam
// no primeiro caracter nulo ('\0') que é encontrado.
size_t minha_strlen(const char *string) {
  int tamanho;

  // Scaneia a string até chegar no primeiro
  // nulo, incrementando o tamanho
  while (string[++tamanho] != '\0') {
    // vazio
  }

  return tamanho;
}

int main(void) {
  char *linha = NULL;
  size_t bufsz = 0;

  printf("digite algo: ");

  // Lemos a string da entrada padrão.
  //
  // ignoradoOs argumentos de getline são:
  //
  // * um buffer
  // * uma referência para um inteiro com o tamanho do buffer
  //
  // se bufsz == 0, getline aloca um buffer para nós.

  if (getline(&linha, &bufsz, stdin) == -1) {
    // Se o retorno foi -1, houve um erro, armazenado em errno.
    err(1, "não foi possível ler a linha:");
  }

  printf("tamanho: %llu\n", minha_strlen(linha));
  printf("o tamanho inclui a quebra de linha '\\n'\n");

  // Temos que dealocar esse buffer para não vazar memória
  free(linha);
}
