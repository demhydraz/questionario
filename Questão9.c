#include <stddef.h>
#include <stdio.h>

int busca_linear(const int *inteiros, size_t tamanho, int chave) {
  // Começamos no índice zero e percorremos até o tamanho,
  // que deve ser exclusivo
  for (size_t i = 0; i < tamanho; i++) {
    // Se a chave nesse índice for a que a procuramos,
    // retornamos o índice
    if (inteiros[i] == chave) {
      return i;
    }
  }

  // Se não, não foi encontrada
  return -1;
}

#define TAMANHO 7

int main(void) {
  // Exemplo de uso:
  int inteiros_ex[TAMANHO] = { -1, 10, 30, 4, 6, 8, 9 };
  printf("%d\n", busca_linear(inteiros_ex, TAMANHO, 6));
}
