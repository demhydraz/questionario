#include <stdlib.h>
#include <stdio.h>

// Inverte uma string em espaço constante
// e tempo linear
void inverte_no_lugar(char *string) {
  // Atravessamos a string "começando" das duas pontas,
  // tomando cuidado para não mover o NULL no final da string
  // (por isso strlen - 1);
  int i = 0, j = strlen(string) - 1;

  // Até os índices "se baterem" no meio,
  while (i < j) {
    // troca o caractere i pelo j
    char temp = string[i];
    string[i] = string[j];
    string[j] = temp;

    // aumenta o i e diminui o j para eles se encontrarem
    // no meio.
    i++, j--;
  }

}
// A evolução de inverter "abcde" é assim:
// i = 0, j = 5, string = abcde
// i = 1, j = 4, string = ebcda
// i = 2, j = 3, string = edcba
// i = 3, j = 3, string = edcba

int main(int argc, char *argv[]) {
  char str[] = "minha string";
  inverte_no_lugar(str);
  printf("%s\n", str);
}
