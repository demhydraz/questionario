#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

// Solução iterativa para calcular o n-ésimo termo da sequência de
// Fibonacci.
//
// Ao contrário da solução recursiva, esta é _stack safe_, e não
// term risco de ocorrer um *stack* overflow.
unsigned long fibonacci(int numero) {
  assert(numero >= 1);

  // Começamos com o primeiro e segundo termo
  // (zerésimo e primeiro..?)
  int termo0 = 0, termo1 = 1;

  // Até chegarmos no número especificado,
  while (numero-- > 0) {
    // as variáveis termo0 e termo1 armazenam o "atual" e "próximo"
    // termos da sequência. Para calcular o "próximo próximo", primeiro:
    //
    // * salvamos o valor que será o novo termo atual, que é o próximo
    // termo
    int atual = termo1;
    // * o próximo termo = o termo atual + o termo anterior
    termo1 = atual + termo0;
    // * trocamos o termo atual que salvamos.
    termo0 = atual;
  }

  return termo0;
}

int main(int argc, char *argv[]) {
  printf("%d\n", argc);
  if (argc <= 1) {
    puts("Por favor dê o número do termo que você deseja calcular como entrada na linha de comando");
    return 1;
  }

  int num;
  if ((num = strtol(argv[1], NULL, 10)) == 0) {
    puts("Entre um número diferente de zero ou válido");
  }

  printf("%lu\n", fibonacci(num));
}
